from flask import Flask, jsonify, request
from neo4j import GraphDatabase
from dotenv import load_dotenv

import os

app = Flask(__name__)

load_dotenv()
uri = os.getenv("URI")
username = os.getenv("USER")
password = os.getenv("PASSWORD")
driver = GraphDatabase.driver(uri, auth=(username, password))


##podpunkt3
def get_emp(tx, req_arg):
    sortBy = req_arg.get("sortBy")
    sortOrder = req_arg.get("sortOrder")
    sortDirection = sortOrder.upper() if sortOrder and sortOrder.upper() in ["ASC", "DESC"] else "ASC"
    sortBy = sortBy if sortBy and sortBy in ["first_name", "last_name", "position"] else "first_name"
    req_keys = req_arg.keys()
    filter_fields = ["first_name", "last_name", "position"]
    filters = []

    for field in filter_fields:
        if field in req_keys:
            filters.append(f"e.{field}='{req_arg[field]}'")

    sort_clause = f"ORDER BY e.{sortBy} {sortDirection}" if sortBy else ""

    query = "MATCH (e:Employee)"
    if filters:
        query += f" WHERE {' AND '.join(filters)}"
    query += f" RETURN e {sort_clause}"

    results = tx.run(query).data()
    employees = [result['e'] for result in results]
    return employees


##podpunkt3
@app.route('/employees', methods=['GET'])
def return_employee_route():
    req_arg = request.args
    with driver.session() as session:
        employees = session.read_transaction(get_emp, req_arg)
    response = {"employees": employees}
    return jsonify(response), 200

##podpunkt4
def check_employee_uniqueness(tx, first_name, last_name):
    query = f"MATCH (e:Employee {{first_name: '{first_name}', last_name: '{last_name}'}}) RETURN e"
    result = tx.run(query).data()
    return len(result) == 0

def add_employee(tx, employee_data):
    query = (
        "CREATE (e:Employee {"
        f"first_name: '{employee_data['first_name']}', "
        f"last_name: '{employee_data['last_name']}', "
        f"position: '{employee_data['position']}'"
        "})"
    )
    tx.run(query)
##podpunkt4
@app.route('/employees', methods=['POST'])
def add_employee_route():
    data = request.get_json()
    with driver.session() as session:
        uniqueness_check = session.read_transaction(check_employee_uniqueness, data['first_name'], data['last_name'])
        if uniqueness_check:
            session.write_transaction(add_employee, data)
            response = {"message": "Employee added successfully"}
            return jsonify(response), 201
        else:
            response = {"error": "Employee with the same name already exists"}
            return jsonify(response), 400
##podpunkt5
def employee_exists_by_id(tx, id):
    query_check = "MATCH (e:Employee) WHERE ID(e) = $id RETURN e"
    return tx.run(query_check, id=id).single() is not None

def update_employees(tx, id, data):
    employee_fields = ["name", "surname", "position"]
    fields_to_update = []
    for employee_field in employee_fields:
        if employee_field in data:
            fields_to_update.append(f"e.{employee_field}='{data[employee_field]}'")
    query = """MATCH (e:Employee)-[oldRel:WORKS_IN]->(oldDepartment:Department)
            WHERE ID(e) = $id"""
    if data["department"]:
        department = data["department"]
        query+=f"""
                DELETE oldRel
                WITH e
                MERGE (newDepartment:Department {{name: "{department}"}})
                MERGE (e)-[:WORKS_IN]->(newDepartment)
                """
    if fields_to_update:
        query += f" SET {', '.join(fields_to_update)}"

    query += f" RETURN e"

    tx.run(query, id=id)
##podpunkt5
@app.route("/employees/<int:id>", methods = ["PUT"])
def put_employees_route(id):
    data = request.get_json()
    print(id)
    print(data)
    if id and data:
        with driver.session() as session:
            if not employee_exists_by_id(session, id):
                response = {"message": "Employee doesn't exist!"}
                return jsonify(response), 404
            else:
                session.write_transaction(update_employees, id, data)
                response = {'success': "Employee updated successfully"}
                return jsonify(response), 200
    else:
        response = {"message": "Invalid request input!"}
        return jsonify(response), 405
    
##podpunkt6
def delete_employee(tx, employee_id):
    query = f"MATCH (e:Employee) WHERE ID(e) = {employee_id} DETACH DELETE e"
    tx.run(query)
##podpunkt6
@app.route('/employees/<int:employee_id>', methods=['DELETE'])
def delete_employee_route(employee_id):
    with driver.session() as session:
        session.write_transaction(delete_employee, employee_id)
        response = {"message": "Employee deleted successfully"}
        return jsonify(response), 200
##podpunkt7
def get_subordinates(tx, id):
    query = """
            MATCH (e:Employee)-[:MANAGES]->(d:Department) 
            WHERE ID(e) = $id
            MATCH (employee:Employee)-[:WORKS_IN]->(d)
            RETURN employee
            """
    results = tx.run(query, id=id).data()
    employees = [result['employee'] for result in results]
    return employees
##podpunkt7
@app.route('/employees/<int:employee_id>/subordinates', methods=['GET'])
def get_subordinates_route(employee_id):
    with driver.session() as session:
        employees = session.read_transaction(get_subordinates, employee_id)
        response = jsonify({"message": employees})
        return response, 200
##podpunkt8
def get_department_info(tx, employee_id):
    query = """
            MATCH (employee:Employee)-[:WORKS_IN]->(d:Department)
            WHERE ID(employee) = $id
            MATCH (e:Employee)-[:WORKS_IN]->(d) 
            MATCH (m:Employee)-[:MANAGES]->(d)
            RETURN d.name AS name, COUNT(e) AS `number of employees`, m AS manager
            """
    department = tx.run(query, id=employee_id).data()[0]
    return department
##podpunkt8
@app.route('/employees/<int:employee_id>/departments', methods=['GET'])
def get_department_info_route(employee_id):
    with driver.session() as session:
        department_info = session.read_transaction(get_department_info, employee_id)
        response = {"department_info": department_info}
        return jsonify(response), 200
##podpunkt9
def get_departments(tx, request_arguments):
    sortBy = request_arguments.get("sortBy")
    sortOrder = request_arguments.get("sortOrder")
    sortDirection = sortOrder.upper() if sortOrder and sortOrder.upper() in ["ASC", "DESC"] else "ASC"
    request_keys = request_arguments.keys()
    filter_fields = ["name", "number_of_employees", "manager.name", "manager.surname", "manager.position"]
    filters = []
    for field in filter_fields:
        if field in request_keys:
            filters.append(f"{field}='{request_arguments[field]}'")
    sort_clause = f"ORDER BY {sortBy} {sortDirection}" if sortBy else ""
    query = """
            MATCH (d:Department)
            MATCH (e:Employee)-[:WORKS_IN]->(d) 
            MATCH (m:Employee)-[:MANAGES]->(d)
            WITH d.name AS name, COUNT(e) AS number_of_employees, m AS manager
            """
    if filters:
        query += f" WHERE {' AND '.join(filters)}"
    query += f" RETURN name, number_of_employees, manager {sort_clause}"
    results = tx.run(query).data()
    return results
##podpunkt9
@app.route('/departments', methods=['GET'])
def get_departments_route():
    req_args = request.args
    with driver.session() as session:
        departments = session.read_transaction(get_departments, req_args)
        response = {"departments": departments}
        return jsonify(response), 200
    
#podpunkt10
def get_employees_from_department(tx, id):
    query = "MATCH (e:Employee)-[:MANAGES|WORKS_IN]->(d:Department) WHERE ID(d)=$id RETURN e"
    results = tx.run(query, id=id).data()
    employees = [{'first_name': result['e']['first_name'], 'last_name': result['e']['last_name'], 'position': result['e']['position']} for result in results]
    return employees

#podpunkt10
@app.route('/departments/<int:department_id>/employees', methods=['GET'])
def get_department_employees_route(department_id):
    with driver.session() as session:
        employees = session.read_transaction(get_employees_from_department, department_id)
        response = {"employees": employees}
        return jsonify(response), 200


if __name__ == '__main__':
    app.run()